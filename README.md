Woggle
======

A Boggle clone for android.

Boggle is a word game that has 16 dies with certain letters that are randomly  arranged in a 4x4 grid.
A player scores points by making words from a path on the board, include diagonals.
No die may be used twice in the same word, and words must be at least 3 characters long.

The game contains a pruned dictionary, that can be found on most UNIX systems.
$ cat /usr/share/dict/words | awk '2<length($0) && length($0)<17' > dict.txt

Matthew Williamson
mwilli20@binghamton.edu
