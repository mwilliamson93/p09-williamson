package edu.binghamton.cs.hello;

import android.content.Context;

public class Player {
	private final String name;
	private int score;
	private float progress;
	private final float total;
	private ProgressWheel wheel;

	public Player(Context context, String name, float total) {
		this.name = name;
		this.score = 0;
		this.progress = 0;
		this.total = total;
		this.wheel = new ProgressWheel(context, total);
	}

	public String name() {
		return this.name;
	}

	public int score() {
		return this.score;
	}

	public void add(int points) {
		this.score += points;
	}

	public boolean done() {
		return this.total <= this.progress;
	}

	public ProgressWheel wheel() {
		return this.wheel;
	}

	public void tick() {
		this.progress++;
		this.wheel.tick(this.progress);
	}

	public void reset() {
		this.score = 0;
		this.progress = 0;
		this.wheel.tick(this.progress);
	}
}